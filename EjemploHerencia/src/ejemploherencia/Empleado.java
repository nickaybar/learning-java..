/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemploherencia;

public class Empleado extends Persona {
    
    private int idEmpleado;
    private double sueldo;
    private static int contadorEmpleados;
    
    
    public Empleado (String nombre, double sueldo)
    {
        super(nombre); // Se utiliza para inicializar el nombre de la clase padre ( persona)
        this.idEmpleado = ++contadorEmpleados; // Primero se incrementa el valor, y luego se asigna
        this.sueldo = sueldo;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }
    
    
    @Override
    public String toString(){
        return super.toString() + " Empleado{" + "idEmpleado=" + idEmpleado + ", sueldo=" + sueldo + "}";
    }
    
    
}
