/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemploherencia;

/**
 *
 * @author nicoa
 */
public class EjemploHerencia {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Empleado e1 = new Empleado("Juan", 25000);
        System.out.println("Imprimimos el objeto Empleado1");
        System.out.println(e1);
        
        Empleado e2 = new Empleado ("Pedro", 15000);
        e2.setEdad(28);
        e2.setGenero('M');
        e2.setDireccion("Bogota, Colombia");
        System.out.println("\nImprimimos el objeto Empleado2");
        System.out.println(e2);
        
        //Creamos los clientes
        
        Cliente c1 = new Cliente(new java.util.Date(), false);
        System.out.println("\nImprimimos el objeto Cliente1");
        System.out.println(c1);
        //Completamos el objeto Cliente
        c1.setNombre("Marcelo");
        c1.setEdad(22);
        c1.setGenero('M');
        c1.setDireccion("Caracas, Venezuela");
        c1.setVip(true);
        System.out.println("\nImprimimos el objeto Cliente1 Modificado\n" +c1);
    }
    
}
