/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package farmacia;

/**
 *
 * @author nicoa
 */
public class ListadoProductos {
    
    String[] productos = new String[] {"Ibuprofeno", "Tafirol", "Aspirina"};
    double[] precios = new double[] {100,80,50};
    
    String productoElegido = "";
    double precioElegido=0;

    public ListadoProductos() 
    {
       
    }
    
    public ListadoProductos(String[] productos, double[] precios)
    {
        this.productos = productos;
        this.precios = precios;
    }

    public String getProductoElegido(int opcion) 
    {  
        this.productoElegido = productos[opcion];
        return productoElegido;
    }

    public double getPrecioElegido(int opcion)
    {  
        this.precioElegido = precios[opcion];
        return precioElegido;
    }

    public String[] getListaProductos() {
        return productos;
    }
    
    
    
        
}
