

package farmacia;

import java.util.Scanner;
public class Farmacia 
{
    public static void MenuFarmacia(){
      System.out.println("---MENU FARMACIA---");
      System.out.println("1- Vender Un Producto");
      System.out.println("2- Mostrar Ventas");
      System.out.println("3- Mostrar Ganancias");
      System.out.println("4- Estadisticas Obra Sociales");
      System.out.println("5- Producto +/- Vendido");
      System.out.println("0- Salir");
      System.out.print("\nSeleccione una opcion: ");
}

    public static void MenuObras()
    {  
        ListadoObraSocial obraSocial = new ListadoObraSocial();
        int[] contadorObras = new int[obraSocial.obrasSociales.length];
        String[] nombreObras = obraSocial.getObrasSociales();
        
        
        System.out.println("\nSeleccione la obra social del cliente");
        
        for (int i = 0; i < nombreObras.length; i++) 
        {
             System.out.println(i+1+"- " + nombreObras[i]);
        }
        System.out.print("\nSeleccione una opcion: ");
    }
    
    
    public static void ListaProductos()
    {
        
    ListadoProductos lista = new ListadoProductos();
    String[] listado = lista.getListaProductos();
    
        for (int i = 0; i < listado.length; i++) 
        {
             System.out.println(i+1+"- " + listado[i]);
        }
        
        System.out.print("\nSeleccione el item a vender: ");
    }
    
    public static double DescuentoObraSocial(double precio, double descuento)
    {
        double precioFinal= 0.0;
        
        precioFinal = precio - ((descuento / 100) * precio);
        
        return precioFinal;
    }
    
   
    public static int[] ContadorObraSocial(int[] estadisticas, int opcion) 
    {
        estadisticas[opcion]++;
        return estadisticas;
    }
    
    public static void EstadisticasObraSocial(int[] estadisticas)
    {
        
        ListadoObraSocial obraSocial = new ListadoObraSocial();
        String[] nombreObras = obraSocial.getObrasSociales();
         
        
        System.out.println("\nEstadisticas obra sociales: ");
        System.out.println("\n");
        for (int i = 0; i < nombreObras.length; i++) 
        {
             System.out.println(i+1+"- " + nombreObras[i] + ": "+ estadisticas[i]);
        }
        System.out.println("");
    }
    
    public static double[] GananciasObraSocial(double[] gananciasObraSocial, double venta, int opcionObras)
    {
        
        gananciasObraSocial[opcionObras] += venta;
        
        return gananciasObraSocial;
    }
    
    public static void MostrarGananciasObraSocial(String[] nombresObras, double[] gananciasObraSocial)
    {
        
        System.out.println("\nGanancias Obra Sociales: ");
        System.out.println("\n");
        for (int i = 0; i < nombresObras.length; i++) 
        {
             System.out.println(i+1+"- " + nombresObras[i] + ": $"+ gananciasObraSocial[i]);
        }
        System.out.println("");
        
    }
    
    public static void main(String[] args)
    {
        //Variables de inicializacion
        int opcion = 0;
        int opcionMenu=0;
        int opcionObras=0;
        Scanner entradaDatos = new Scanner(System.in);
        
        
        //Variables de proceso
        String producto = "";
        String obra= "";
        double precio=0;
        double precioFinal=0;
        double descuento=0;
        double ganancia=0;
        
        
        //Variables de resultados
        double ganancias=0;
        double promedio =0;
        int numeroVentas=0;
        
        
        //Objetos & arrays
        ListadoProductos items = new ListadoProductos();
        ListadoObraSocial obraSocial = new ListadoObraSocial();
        
        
        //declaracion de arrays vacios
        int[] estadisticasVentasObraSocial = new int[obraSocial.obrasSociales.length];
        double[] gananciasObraSocial= new double[obraSocial.obrasSociales.length];
        String[] listaObraSocial = new String[obraSocial.obrasSociales.length];
        listaObraSocial = obraSocial.getObrasSociales();
        
        
        //Ejecucion
        
        do
        {
            
             MenuFarmacia();
             opcionMenu = entradaDatos.nextInt();
             switch(opcionMenu )
             {

                    case 1 :
                        System.out.println("");
                        ListaProductos();
                        opcion = entradaDatos.nextInt()-1;
                        producto = items.getProductoElegido(opcion);
                        precio = items.getPrecioElegido(opcion);
                        MenuObras();
                        opcionObras = entradaDatos.nextInt()-1;
                        obra = obraSocial.getObraSocialElegida(opcionObras);
                        estadisticasVentasObraSocial = ContadorObraSocial(estadisticasVentasObraSocial, opcionObras); //numero de ventas obra social c/u
                        descuento = obraSocial.getDescuentoElegido(opcionObras);
                        precioFinal = DescuentoObraSocial(precio, descuento);
                        gananciasObraSocial= GananciasObraSocial(gananciasObraSocial, precioFinal, opcionObras); // ganancias obra social c/u
                        ganancia += precioFinal; // Ganancia General
                        numeroVentas++; // Numero ventas general
                        System.out.println("La ganancia es de $" + ganancia);
                        System.out.println("\n\n\n");
                 break;
                 
                    case 2:
                       
                        MostrarGananciasObraSocial(listaObraSocial, gananciasObraSocial);
                        break;
                        
                    case 4:
                        EstadisticasObraSocial(estadisticasVentasObraSocial);
                        
                 break;
                        
             }
        }
        while(opcionMenu !=0);
    }
}
