/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package farmacia;

/**
 *
 * @author nicoa
 */
public class ListadoObraSocial

{
    String[] obrasSociales = new String[] {"Prensa", "Galeno", "Sin Obra Social"};
    double[] descuento = new double[] {15,10,0};
    
    String obraSocialElegida = "";
    double descuentoElegido=0;

    public ListadoObraSocial() 
    {
        
    }
    
     public ListadoObraSocial(String[] obra, double[] precio) 
     
    {
       this.obrasSociales = obra;
       this.descuento = precio;
    }
    public String getObraSocialElegida(int opcion) 
    
    {
        this.obraSocialElegida = obrasSociales[opcion];
        return obraSocialElegida;
    }

    public double getDescuentoElegido(int opcion) 
    {
        this.descuentoElegido = descuento[opcion];
        return descuentoElegido;
    }

    public String[] getObrasSociales() {
        return obrasSociales;
    }
}
