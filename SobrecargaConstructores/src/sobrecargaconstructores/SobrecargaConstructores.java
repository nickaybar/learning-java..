/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sobrecargaconstructores;

/**
 *
 * @author nicoa
 */
public class SobrecargaConstructores {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Persona p1 = new Persona("Lilia", 22);
        
        System.out.println("Imprimimos el objeto P1");
        System.out.println(p1);
        
        Persona p2 = new Persona("Juan", 23);
        System.out.println("\nObjeto p2");
        System.out.println(p2);
        
        
        Empleado e1= new Empleado("Pedro", 29, 18000);
        System.out.println("\nObjeto empleado e1");
        System.out.println(e1);
    }
    
}
