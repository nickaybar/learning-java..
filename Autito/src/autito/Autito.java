/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package autito;

import java.util.Scanner;

public class Autito {

    public static void main(String[] args)
    {
       //Variables creadas
       Auto p = new Auto(); // Creacion del objeto.
      
       String nombre
               ;
       String modelo;
       String color;
       String puertas;
       
       Modelos modelos = new Modelos();
       Puertas puerta = new Puertas();
       Colores colorElegido = new Colores();

        int seleccionModelo;
        int seleccionColor;
        int seleccionPuertas;
     
        Scanner entradaDatos = new Scanner(System.in);  
        
        
        //Ejecucion del programa
        System.out.print("Ingrese el nombre del usuario: ");
        nombre = entradaDatos.nextLine();
        System.out.println("Modelos de auto \n1- Nissan \n2- Toyota \n3- Fiat");
        System.out.print("Seleccione la opcion: ");  
        seleccionModelo= entradaDatos.nextInt()-1;
        modelo = modelos.getModelos(seleccionModelo);
        System.out.print("Elija cuantas puertas desea\n\n1- 2 Puertas\n2- 4 Puertas\nSeleccione la opcion: ");
        seleccionPuertas = entradaDatos.nextInt()-1;
        puertas = puerta.getPuertas(seleccionPuertas); 
        System.out.print("Elija el color del auto\n\n1- Rojo\n2- Azul\nSeleccione la opcion: ");
        seleccionColor = entradaDatos.nextInt()-1;
        color = colorElegido.getColores(seleccionColor);
        
        //Mostrar resultados
        p.desplegarResultados(nombre, modelo, color, puertas);

    } 
}
