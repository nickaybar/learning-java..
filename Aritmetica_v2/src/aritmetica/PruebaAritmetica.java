/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aritmetica;

/**
 *
 * @author nicoa
 */
public class PruebaAritmetica {
    public static void main (String args []){
      
      // Creacion de objeto en la clase arimetica con el constr vacio
      Aritmetica obj1 = new Aritmetica();
      
      int resultado = obj1.sumar(5,4);
      
      System.out.println("Resultado de suma directa obj1: " +resultado);
      
      //Si llamamos el metodo sumar directamente sin argumentos
      //El valor es cero, ya que los atributos del objeto nunca se inicializaron
      //Ya que no se uso el constructor con argumentos, sino el constructor vacio
      
      System.out.println("Resultado suma atributos obj1 " +obj1.sumar());
      
      //Ahora creamos un objeto con el constructor con 2 argumentos
      Aritmetica obj2 = new Aritmetica(2,1);
      
      //Imprimimos directamente el resultado
      //Al llamar directamente al metodo suma, nos regresa el valor de la suma
      System.out.println("\n Resultado suma atributos ojb2: " +obj2.sumar());
             
      Aritmetica obj3 = new Aritmetica();
      int resultado2 = obj3.sumar(10,5);
      
      System.out.println("Resultado aritmetica: " + resultado2);
      
              
        
    }
}
