/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplopalabrafinal;

/**
 *
 * @author nicoa
 */
public class EjemploPalabraFinal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        System.out.println("Nueva persona: " + ClaseFinal.persona.getNombre());
        
        
        ClaseFinal.persona.setNombre("otro");
        
        
        System.out.println("Nombre persona: " + ClaseFinal.persona.getNombre());
    }
    
}
