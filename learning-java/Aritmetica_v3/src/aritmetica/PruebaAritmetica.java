/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aritmetica;
import java.util.Scanner;
/**
 *
 * @author nicoa
 */
public class PruebaAritmetica {
    public static void main (String args []){
    
    //variables
    double operadorA=0;
    double operadorB=0;

    Scanner entrada = new Scanner(System.in);

    System.out.println("Ingrese el valor de A: ");
    operadorA = entrada.nextInt();
    System.out.println("Ingrese el valor de B: ");
    operadorB= entrada.nextInt();
      
    Aritmetica obj1 = new Aritmetica(operadorA, operadorB);
      
    System.out.println("Resultado suma = " + obj1.sumar());
    System.out.println("Resultado resta = " + obj1.restar());
    System.out.println("Resultado multiplicacion = " + obj1.multiplicar());
    System.out.println("Resultado division = " + obj1.dividir());
    }
}
