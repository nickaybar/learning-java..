/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aritmetica;

public class Aritmetica {
    
    //Atributos de la clase
    double a;
    double b;
    
    //Constructor Vacio
    //Recordar que si agregamos un constructor distinto al vacio
    //ya no se crea este constructor y nosotros debemos crearlo si lo necesitamos
    Aritmetica(){}
    
    //Constructor con 2 argumentosn
    Aritmetica( double a , double b){
        //Uso del operador this
        this.a = a;
        this.b = b;
    }
    
    //Este metodo toma los atributos de la clase para hacer la suma
    double sumar(){
        return a + b;
    }
    
    //Metodo restar
    double restar(){
        return a - b;
    }
    
    //Metodo multiplicar
    double multiplicar(){
        return a * b;
    }
    
    //Metodo dividir
    double dividir(){
        return a / b;
    }
}
