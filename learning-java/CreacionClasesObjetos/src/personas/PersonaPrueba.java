/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package personas;

/**
 *
 * @author nicoa
 */
public class PersonaPrueba {
    public static void main (String args[]){
        Persona p1 = new Persona();
        Persona p2 = new Persona();
        
        //Llamado a un metodo el objeto creado
        System.out.println("Valores por default del objeto personas");
        p1.desplegarNombre();
        
        //Modificar valores
        p1.nombre = "Juan";
        p1.apellidoMaterno = "Esparza";
        p1.apellidoPaterno="Rodriguez";
        p2.nombre = "Jose";
        p2.apellidoMaterno = "Naigonlan";
        p2.apellidoPaterno = "Rubio";
        
        //volvemos a llamar al metodo
        System.out.println("\n Nuevos valores del objeto Persona");
        p1.desplegarNombre();
        p2.desplegarNombre();
    }
}
