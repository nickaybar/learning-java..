/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciclos;

import java.util.Scanner;
/**
 *
 * @author nicoa
 */
public class CicloFor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int limite;
        System.out.print("Ingrese el limite del for: ");
        Scanner entradaLimite = new Scanner(System.in);
        limite = entradaLimite.nextInt();
        for (int i = 0; i < limite; i++) {
            System.out.println("Contador = " + i);
        }
    }
    
}
