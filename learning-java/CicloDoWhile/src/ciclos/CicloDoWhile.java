/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciclos;

import java.util.Scanner;
/**
 *
 * @author nicoa
 */
public class CicloDoWhile {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int contador = 0;
        int limite;
        System.out.print("Por favor introduzca el limite del numero: ");
        Scanner entradaDatos = new Scanner(System.in);
        
        limite = entradaDatos.nextInt();
        do{
            System.out.println("Contador = " + contador);
            contador++;
        }while(contador<limite);
    }
}
