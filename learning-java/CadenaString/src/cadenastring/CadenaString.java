/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadenastring;

/**
 *
 * @author nicoa
 */
public class CadenaString 
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
        
        String nombre = "Karla";
        String apellido = "Esparza";
        
        System.out.println("Concadenacion: " + nombre + " "+ apellido);
        System.out.println("Nueva linea:\n" + nombre + " " + apellido);
        ///
        System.out.println("Tabulador: \t" + nombre + " " + apellido);
        System.out.println("Retroceso: \b" + nombre + " " + apellido);
    }
}
